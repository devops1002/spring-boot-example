package com.example.sample.client;

import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * TODO REPLACE_ME 示例服务
 */
@Slf4j
public class SampleClientTest {
    @BeforeClass
    public static void init() {
        System.out.println("test env init ...");
    }

    @Test
    public void test_create_sample_equal() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void test_get_sample_equal() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void test_delete_sample_equal() {
        Assert.assertEquals(1, 1);
    }

    @AfterClass
    public static void destroy() throws IOException {
        System.out.println("test env destroied");
    }
}
