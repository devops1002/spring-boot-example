package com.example.sample.service;

import com.example.sample.domain.Sample;

/**
 * TODO REPLACE_ME 示例服务
 */
public interface SampleService {

  Sample getSample(Long id);

  Long createSample(Sample sample);

  void deleteSample(Long id);

}
