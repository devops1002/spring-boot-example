package com.example.sample.internal.service;

import com.example.sample.domain.Sample;
import com.example.sample.service.SampleService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.LongAdder;

/**
 * TODO REPLACE_ME 示例服务
 */
@Service
@Slf4j
public class SampleServiceImpl implements SampleService {

  private LongAdder longAdder = new LongAdder();

  @Override
  public Sample getSample(Long id) {
    return new Sample(id, "Name" + id);
  }

  @Override
  public Long createSample(Sample sample) {
    longAdder.increment();
    Long id = longAdder.longValue();
    log.info("Create sample, new id = {}", id);
    return id;
  }

  @Override
  public void deleteSample(Long id) {
    log.info("Delete sample by id = {}", id);


  }
}
