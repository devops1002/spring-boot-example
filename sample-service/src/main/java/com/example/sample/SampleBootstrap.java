package com.example.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * TODO REPLACE_ME 替换package name
 */
@SpringBootApplication(scanBasePackages = {"com.example.sample"})
@EnableAspectJAutoProxy
public class SampleBootstrap {

  public static void main(String[] args) throws Exception {
    SpringApplication.run(SampleBootstrap.class, args);
  }

}
