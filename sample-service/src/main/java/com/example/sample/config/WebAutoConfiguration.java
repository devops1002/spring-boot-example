package com.example.sample.config;

import com.example.platform.accesslog.springboot.EmbeddedTomcatCustomizer;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebAutoConfiguration implements WebMvcConfigurer {

  @Bean // access-log
  public WebServerFactoryCustomizer tomcatAccessLogCustomizer() {
    return new EmbeddedTomcatCustomizer(true /*logResponseSize or not*/);
  }
}
