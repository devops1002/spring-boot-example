package com.example.sample.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * TODO REPLACE_ME 替换ERROR_CODE
 */
@RequiredArgsConstructor
public enum ErrorCode {

  INVALID_PARAMETER("SAMPLE_INVALID_PARAMETER", "Invalid parameters");

  @Getter
  private final String code;
  @Getter
  private final String description;

}