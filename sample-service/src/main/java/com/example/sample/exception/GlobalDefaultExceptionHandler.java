package com.example.sample.exception;

import com.example.platform.common.exception.ExceptionResponse;
import com.example.platform.common.exception.spring.CommonExceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalDefaultExceptionHandler extends CommonExceptionHandler {

  // handle your own exceptions

  @Override
  // could be override
  protected ResponseEntity<ExceptionResponse> handleServiceException(HttpServletRequest request, HttpStatus status, String errorCode, String errorMessage, Throwable ex) {
    return super.handleServiceException(request, status, errorCode, errorMessage, ex);
  }

  @Override
  // could be override
  protected ResponseEntity<ExceptionResponse> handleSystemException(HttpServletRequest request, HttpStatus status, String errorMessage, Throwable ex) {
    return super.handleSystemException(request, status, errorMessage, ex);
  }
}
