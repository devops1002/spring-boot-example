package com.example.sample.util.feign;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Getter
public class HttpClientManager implements Closeable {

  private final CloseableHttpClient client;
  private final PoolingHttpClientConnectionManager connectionManager;
  private final IdleConnectionMonitorThread monitorThread;

  public HttpClientManager(int connectionTimeout, int readTimeout,
                           int maxTotalConnections, int maxPerRouteConnections) {
    this.connectionManager = createConnectionManager(maxTotalConnections, maxPerRouteConnections);
    this.client = createClient(connectionManager, connectionTimeout, readTimeout);
    this.monitorThread = new IdleConnectionMonitorThread(connectionManager);
    this.monitorThread.setDaemon(true);
    this.monitorThread.start();
    log.info("Create pooling http client manager, connectionTimeout = {}, readTimeout = {}, maxTotal = " +
            "{}, maxPerRouteConnections= {}",
        connectionTimeout, readTimeout, maxTotalConnections, maxPerRouteConnections);
  }

  private CloseableHttpClient createClient(PoolingHttpClientConnectionManager connectionManager,
                                           int soTimeout, int connectionTimeout) {
    final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(soTimeout)
        .setConnectTimeout(connectionTimeout).build();

    final SocketConfig defaultSocketConfig = SocketConfig.custom().setSoKeepAlive(true)
        .setTcpNoDelay(true).setSoReuseAddress(true).build();
    return HttpClients.custom().setConnectionManager(connectionManager).useSystemProperties()
        .setDefaultRequestConfig(requestConfig).setDefaultSocketConfig(defaultSocketConfig)
        .setRetryHandler(new DefaultHttpRequestRetryHandler(0, false)).build();
  }

  private PoolingHttpClientConnectionManager createConnectionManager(int maxTotalConnections,
                                                                     int maxPerRouteConnections) {
    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
    connectionManager.setMaxTotal(maxTotalConnections);
    connectionManager.setDefaultMaxPerRoute(maxPerRouteConnections);
    return connectionManager;
  }

  @Override
  public void close() throws IOException {
    if (this.monitorThread != null) {
      this.monitorThread.shutdown();
    }
    // Ensure that all connections kept alive by the manager get closed and release system resources
    if (this.client != null) {
      this.client.close();
    }
  }

  public PoolStats getTotalStats() {
    if (this.connectionManager != null) {
      return this.connectionManager.getTotalStats();
    }
    return null;
  }

  public static class IdleConnectionMonitorThread extends Thread {

    private final HttpClientConnectionManager connMgr;
    private volatile boolean shutdown;
    private final Object obj = new Object();

    public IdleConnectionMonitorThread(HttpClientConnectionManager connMgr) {
      super();
      this.connMgr = connMgr;
    }

    @Override
    public void run() {
      try {
        while (!shutdown) {
          synchronized (obj) {
            obj.wait(5000);
            // Close expired connections
            connMgr.closeExpiredConnections();
            // Optionally, close connections
            // that have been idle longer than 30 sec
            connMgr.closeIdleConnections(30, TimeUnit.SECONDS);
          }
        }
      } catch (InterruptedException ex) {
      }
    }

    public void shutdown() {
      shutdown = true;
      synchronized (obj) {
        obj.notifyAll();
      }
    }
  }
}
