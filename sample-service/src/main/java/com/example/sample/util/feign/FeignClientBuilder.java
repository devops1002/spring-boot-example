package com.example.sample.util.feign;

import com.google.common.collect.Maps;

import com.example.platform.common.exception.feign.FeignClientErrorDecoder;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.apache.http.impl.client.CloseableHttpClient;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import feign.Contract;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.RequestInterceptor;
import feign.Retryer;
import feign.Target;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;

@Slf4j
@RequiredArgsConstructor
public class FeignClientBuilder implements Closeable {

    private final String appId;

    private static final Map<String, HttpClientManager> HTTP_CM_CONTAINER = Maps.newConcurrentMap();

    public <T> T buildClient(Class<T> clazz, RemoteAppConfig remoteAppConfig,
        FeignClientConfig clientConfig) {
        FeignClientConfig feignClientConfig =
            clientConfig != null ? clientConfig : new FeignClientConfig();
        return Feign.builder().
            client(feignClientConfig.getClient() != null ? feignClientConfig.getClient() :
                new ApacheHttpClient(createHttpClient(remoteAppConfig))).
            options(new Request.Options(remoteAppConfig.getConnectTimeout(),
                remoteAppConfig.getReadTimeout())).
            logLevel(feignClientConfig.getLoggerLevel() != null ? feignClientConfig.getLoggerLevel()
                : Logger.Level.NONE).
            logger(feignClientConfig.getLogger() != null ? feignClientConfig.getLogger()
                : new Slf4jLogger(clazz)).
            contract(feignClientConfig.getContract() != null ? feignClientConfig.getContract()
                : new Contract.Default()).
            encoder(feignClientConfig.getEncoder() != null ? feignClientConfig.getEncoder()
                : new JacksonEncoder()).
            decoder(feignClientConfig.getDecoder() != null ? feignClientConfig.getDecoder()
                : new JacksonDecoder()).
            retryer(feignClientConfig.getRetryer() != null ? feignClientConfig.getRetryer()
                : Retryer.NEVER_RETRY).
            errorDecoder(
                feignClientConfig.getErrorDecoder() != null ? feignClientConfig.getErrorDecoder()
                    : new FeignClientErrorDecoder()).
            requestInterceptors(defaultInterceptors(feignClientConfig.getRequestInterceptors())).
            target(new Target.HardCodedTarget<>(clazz, remoteAppConfig.getRemoteAppId(),
                remoteAppConfig.getUrl()));
    }

    private List<RequestInterceptor> defaultInterceptors(List<RequestInterceptor>
        extendedInterceptors) {
        List<RequestInterceptor> interceptors = new ArrayList<>();
        // add default http header
        interceptors.add(template -> {
            template.header("Accept", "application/json");
            if (!template.headers().containsKey("Content-Type")) {
                template.header("Content-Type", "application/json;charset=UTF-8");
            }
            template.header("X-Example-App-Id", appId);
        });
        if (extendedInterceptors != null && !extendedInterceptors.isEmpty()) {
            interceptors.addAll(extendedInterceptors);
        }
        return interceptors;
    }

    private CloseableHttpClient createHttpClient(RemoteAppConfig remoteAppConfig) {
        String remoteAppId = remoteAppConfig.getRemoteAppId();
        HttpClientManager cm = HTTP_CM_CONTAINER.get(remoteAppId);
        if (cm != null) {
            return cm.getClient();
        }
        synchronized (remoteAppId.intern()) {
            if (!HTTP_CM_CONTAINER.containsKey(remoteAppId)) {
                cm = new HttpClientManager(remoteAppConfig.getConnectTimeout(),
                    remoteAppConfig.getReadTimeout(),
                    remoteAppConfig.getMaxTotalConnections(),
                    remoteAppConfig.getMaxPerRouteConnections());
                HTTP_CM_CONTAINER.put(remoteAppId, cm);
            }
        }
        return HTTP_CM_CONTAINER.get(remoteAppId).getClient();
    }

    @Override
    public void close() throws IOException {
        HTTP_CM_CONTAINER.forEach((remoteAppId, cm) -> {
            try {
                cm.close();
            } catch (IOException e) {
                log.warn("Close http client manager failed for {}", remoteAppId, e);
            }
        });
    }
}
