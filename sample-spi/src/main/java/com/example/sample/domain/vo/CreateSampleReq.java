package com.example.sample.domain.vo;

/**
 * TODO REPLACE_ME 示例服务
 */
public class CreateSampleReq {
  private String name;

  public CreateSampleReq() {
  }

  public CreateSampleReq(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
