package com.example.sample.client;

import com.example.sample.domain.vo.CreateSampleReq;
import com.example.sample.domain.vo.SampleVo;

import feign.Param;
import feign.RequestLine;

/**
 * TODO REPLACE_ME 示例服务
 */
public interface SampleClient {

  @RequestLine("POST " + "/v1/samples")
  Long createSample(CreateSampleReq createSampleReq);

  @RequestLine("GET " + "/v1/samples/{id}")
  SampleVo getSample(@Param("id") Long id);

  @RequestLine("DELETE " + "/v1/samples/{id}")
  void deleteSample(@Param("id") Long id);
}
