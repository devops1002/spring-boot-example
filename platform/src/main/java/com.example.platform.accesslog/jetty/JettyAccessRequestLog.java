package com.example.platform.accesslog.jetty;

import com.example.platform.accesslog.internal.AccessLogUtil;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.RequestLog;
import org.eclipse.jetty.server.Response;

public class JettyAccessRequestLog implements RequestLog {
    private boolean logResponseSize = true;

    public JettyAccessRequestLog() {
    }

    public void log(Request request, Response response) {
        AccessLogUtil.log(request, response, System.currentTimeMillis() - request.getTimeStamp(), this.logResponseSize ? response.getHttpChannel().getBytesWritten() : -1L);
    }

    public void setLogResponseSize(boolean logResponseSize) {
        this.logResponseSize = logResponseSize;
    }
}