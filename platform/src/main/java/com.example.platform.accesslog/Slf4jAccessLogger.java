package com.example.platform.accesslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jAccessLogger implements AccessLogger {
    private static final Logger LOGGER = LoggerFactory.getLogger("api.accesslog");

    public Slf4jAccessLogger() {
    }

    public String name() {
        return "slf4j";
    }

    public void log(String info) {
        LOGGER.info(info);
    }
}
