package com.example.platform.accesslog.springboot;

import com.example.platform.accesslog.AccessLoggerInstanceHolder;
import com.example.platform.accesslog.Slf4jAccessLogger;
import com.example.platform.accesslog.tomcat.TomcatAccessLogValve;
import org.apache.catalina.Valve;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;

public class EmbeddedTomcatCustomizer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    private final boolean logResponseSize;

    public EmbeddedTomcatCustomizer(boolean logResponseSize) {
        this.logResponseSize = logResponseSize;
        AccessLoggerInstanceHolder.updateAccessLogger(new Slf4jAccessLogger());
    }

    public void customize(ConfigurableServletWebServerFactory container) {
        TomcatServletWebServerFactory c = (TomcatServletWebServerFactory)container;
        TomcatAccessLogValve valve = new TomcatAccessLogValve();
        valve.setLogResponseSize(this.logResponseSize);
        c.addEngineValves(new Valve[]{valve});
    }
}