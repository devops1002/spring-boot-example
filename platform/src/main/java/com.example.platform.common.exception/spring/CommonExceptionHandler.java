package com.example.platform.common.exception.spring;

import com.example.platform.common.exception.AccessDeniedException;
import com.example.platform.common.exception.AuthenticationException;
import com.example.platform.common.exception.ExceptionResponse;
import com.example.platform.common.exception.NotSupportedMethodException;
import com.example.platform.common.exception.ResourceNotFoundException;
import com.example.platform.common.exception.ServiceException;
import com.example.platform.common.exception.SystemException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

public abstract class CommonExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CommonExceptionHandler.class);
    private static final String ERROR_CODE_COMMON_INVALID_PARAMETER = "COMMON_INVALID_PARAMETER";

    public CommonExceptionHandler() {
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, MethodArgumentTypeMismatchException ex) {
        return this.handleServiceException(request, HttpStatus.BAD_REQUEST, "COMMON_INVALID_PARAMETER", ex.getName() + " should be " + ex.getRequiredType(), ex);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, HttpMessageNotReadableException ex) {
        return this.handleServiceException(request, HttpStatus.BAD_REQUEST, "COMMON_INVALID_PARAMETER", ex.getMessage(), ex);
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, MissingServletRequestParameterException ex) {
        return this.handleServiceException(request, HttpStatus.BAD_REQUEST, "COMMON_INVALID_PARAMETER", ex.getParameterName() + " is required", ex);
    }

    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, HttpRequestMethodNotSupportedException ex) {
        return this.handleServiceException(request, HttpStatus.METHOD_NOT_ALLOWED, "COMMON_INVALID_PARAMETER", ex.getMessage(), ex);
    }

    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, ServiceException ex) {
        return this.handleServiceException(request, HttpStatus.BAD_REQUEST, ex.getErrCode(), ex.getErrMsg(), ex);
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, AuthenticationException ex) {
        return this.handleServiceException(request, HttpStatus.UNAUTHORIZED, ex.getErrCode(), ex.getErrMsg(), ex);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, AccessDeniedException ex) {
        return this.handleServiceException(request, HttpStatus.FORBIDDEN, ex.getErrCode(), ex.getErrMsg(), ex);
    }

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, ResourceNotFoundException ex) {
        return this.handleServiceException(request, HttpStatus.NOT_FOUND, ex.getErrCode(), ex.getErrMsg(), ex);
    }

    @ExceptionHandler({NotSupportedMethodException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, NotSupportedMethodException ex) {
        return this.handleServiceException(request, HttpStatus.METHOD_NOT_ALLOWED, ex.getErrCode(), ex.getErrMsg(), ex);
    }

    @ExceptionHandler({SystemException.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, SystemException ex) {
        return this.handleSystemException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error, please try later", ex);
    }

    @ExceptionHandler({Throwable.class})
    public ResponseEntity<ExceptionResponse> exception(HttpServletRequest request, Throwable ex) {
        return this.handleSystemException(request, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error, please try later", ex);
    }

    protected ResponseEntity<ExceptionResponse> handleServiceException(HttpServletRequest request, HttpStatus status, String errorCode, String errorMessage, Throwable ex) {
        LOG.warn("Service exception caught for uri = {}, errCode = {}, errMsg = {}", new Object[]{request.getRequestURI(), errorCode, errorMessage, ex});
        ExceptionResponse res = new ExceptionResponse();
        res.setStatus(status.value());
        res.setErrCode(errorCode);
        res.setErrMsg(errorMessage);
        res.setTimestamp(System.currentTimeMillis());
        res.setException(ex.getClass().getName());
        return ResponseEntity.status(status.value()).body(res);
    }

    protected ResponseEntity<ExceptionResponse> handleSystemException(HttpServletRequest request, HttpStatus status, String errorMessage, Throwable ex) {
        LOG.error("System exception caught for uri = {}, errMsg = {}", new Object[]{request.getRequestURI(), errorMessage, ex});
        ExceptionResponse res = new ExceptionResponse();
        res.setStatus(status.value());
        res.setErrMsg(errorMessage);
        res.setTimestamp(System.currentTimeMillis());
        res.setException(ex.getClass().getName());
        return ResponseEntity.status(status.value()).body(res);
    }
}