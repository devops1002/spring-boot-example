package com.example.platform.common.exception.feign;

import com.example.platform.common.exception.AccessDeniedException;
import com.example.platform.common.exception.AuthenticationException;
import com.example.platform.common.exception.ExceptionResponse;
import com.example.platform.common.exception.NotSupportedMethodException;
import com.example.platform.common.exception.ResourceNotFoundException;
import com.example.platform.common.exception.ServiceException;
import com.example.platform.common.exception.SystemException;
import com.example.platform.common.exception.util.JsonUtil;
import feign.Request;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import feign.codec.ErrorDecoder.Default;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeignClientErrorDecoder implements ErrorDecoder {
    private static final Logger log = LoggerFactory.getLogger(FeignClientErrorDecoder.class);
    private static final ErrorDecoder DEFAULT_DECODER = new Default();

    public FeignClientErrorDecoder() {
    }

    public Exception decode(String methodKey, Response response) {
        Request request = response.request();

        try {
            int status = response.status();
            if (status >= 500) {
                log.error("System error from {}, method is {}, uri is {} status is {}", new Object[]{methodKey, request.method(), request.url(), status});
                return DEFAULT_DECODER.decode(methodKey, response);
            } else if (status >= 400) {
                String bodyContent = Util.toString(response.body().asReader());
                ExceptionResponse exceptionResponse = (ExceptionResponse)JsonUtil.jsonToObject(bodyContent, ExceptionResponse.class);
                switch(status) {
                    case 401:
                        return new AuthenticationException(exceptionResponse.getErrCode(), exceptionResponse.getErrMsg());
                    case 402:
                    default:
                        return new ServiceException(exceptionResponse.getErrCode(), exceptionResponse.getErrMsg());
                    case 403:
                        return new AccessDeniedException(exceptionResponse.getErrCode(), exceptionResponse.getErrMsg());
                    case 404:
                        return new ResourceNotFoundException(exceptionResponse.getErrCode(), exceptionResponse.getErrMsg());
                    case 405:
                        return new NotSupportedMethodException(exceptionResponse.getErrCode(), exceptionResponse.getErrMsg());
                }
            } else {
                return (Exception)(status >= 300 ? new NotSupportedMethodException("UNKNOWN_APP_RESPONSE_STATUS_NOT_SUPPORTED", String.format("Response status %s not supported for method %s", status, methodKey)) : new SystemException(String.format("Unknown error from %s, status is %s", methodKey, status)));
            }
        } catch (IOException var7) {
            log.error("IOException occurs where decode error response", var7);
            return new SystemException(String.format("Decode error occurs when read %s", methodKey), var7);
        }
    }
}
