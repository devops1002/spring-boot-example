package com.example.platform.common.exception;

public class AccessDeniedException extends ServiceException {
    public AccessDeniedException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }

    public AccessDeniedException(String errCode, String errMsg, Throwable cause) {
        super(errCode, errMsg, cause);
    }
}