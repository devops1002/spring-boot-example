package com.example.platform.common.exception;

public class SystemException extends RuntimeException implements BaseException {
    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
