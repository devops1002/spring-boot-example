package com.example.platform.common.exception;

public class NotSupportedMethodException extends ServiceException {
    public NotSupportedMethodException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }

    public NotSupportedMethodException(String errCode, String errMsg, Throwable cause) {
        super(errCode, errMsg, cause);
    }
}